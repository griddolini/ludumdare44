extends Node2D

var use_blood = false

func deployed():
	$BloodRotate.rotation = rand_range(0,180)
	$HitSound.set_pitch_scale(rand_range(.6,1.4))
	$HitSound.play(0)
	$Hit.rotation = rand_range(0,180)
	$Hit.scale = Vector2(rand_range(1,3), 1)
	if use_blood:
		$BloodRotate/Blood.set_visible(true)
		
func _on_TotalDestroy_timeout():
	queue_free()

func _process(delta):
	$Hit.position = Vector2(rand_range(-2,2), rand_range(-2,2))