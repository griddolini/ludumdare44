extends Node2D

func _ready():
	$CPUParticles2D.set_emitting(true)

func _on_Area2D_body_entered(body):
	if body.is_in_group("Enemy"):
		body.get_hurt(20, self.get_global_position())

func _on_Timer_timeout():
	$Area2D.queue_free()
