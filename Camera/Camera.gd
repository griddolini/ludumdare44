extends KinematicBody2D

export var cameraSpeed = 10

export var zoomIncrement := Vector2(0.05,0.05)
export var maxZoom := Vector2(0.7,0.7)
export var minZoom := Vector2(1,1)
var targetZoom := Vector2(0.7,0.7)
var cameraCanMove = false
var motion = Vector2()

func _ready():
	#Save the game camera to the global singleton for easy references
	ResourceManager.camera = get_node("Camera2D")
	
func _process(delta):
	# Smooth out zooming
	$Camera2D.set_zoom(lerp($Camera2D.get_zoom(), targetZoom, 0.2))
	
	var origo = Vector2(get_viewport().size.x/2, get_viewport().size.y/2)
	
	#print(ResourceManager.camera.get_global_mouse_position().normalized())
	if cameraCanMove:
		position = ResourceManager.player.position
		
func _input(event):	
	#move camera while holding mmb
	if event is InputEventMouseMotion and Input.is_action_pressed("MiddleMouseButton"):
		cameraCanMove = false
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		motion = event.relative
		move_and_slide(motion * -1 * cameraSpeed)
		
	elif Input.is_action_just_released("MiddleMouseButton"):
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	else:
		cameraCanMove = true
	# secret zoom ;)
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_WHEEL_UP:
			targetZoom -= zoomIncrement
			if targetZoom.x <= maxZoom.x:
				targetZoom = maxZoom
		if event.button_index == BUTTON_WHEEL_DOWN:
			targetZoom += zoomIncrement
			if targetZoom.x >= minZoom.x:
				targetZoom = minZoom
				
func start_shake(duration = 0.2, frequency = 20, amplitude = 6):
	$Camera2D/ScreenShake.start_shake(duration, frequency, amplitude)

