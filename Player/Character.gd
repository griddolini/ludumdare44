extends "res://SharedScripts/Attackable.gd"

const TYPE = "Player"
signal shakeScreen

export var maxSpeed = 100
export var minSpeed = 30

var velocity = Vector2()
var relativeSpeed
var mousePos
var actionButton
var livingWorkers = 0
var isAttacking = false
var isInvinc = false

var has_released_ability : bool = true
var aiming_ability : bool = false
var selected_ability : int = 0

#onready var spiritLife = get_node("CanvasLayer/HP")

onready var ResourceManager = get_node("/root/ResourceManager")

func _process(delta):
	
	if ResourceManager.total_buildings >= 100:
		Transition.fade_to("res://Stages/WinScreen.tscn")
	
	if current_hp == 0:
		Transition.fade_to("res://Stages/LoseScreen.tscn")
	
	max_hp = 100 + livingWorkers * 2
	
	current_hp = clamp(current_hp, 0, max_hp)
	#spiritLife = current_hp
	
	ResourceManager.hud.set_hp_text(str(current_hp))
	# Load player into singleton for reference
	ResourceManager.player = self
	
	# actionButton is false unless JUST pressed
	actionButton = Input.is_action_just_pressed("ui_accept")
	
	# Follows the mouse
	mousePos = ResourceManager.camera
	var direction = (mousePos.get_global_mouse_position() - position)
	var comparator = direction
	direction = direction.normalized()
	
	var distance = position.distance_to(mousePos.get_global_mouse_position())
	
	#move faster the further away from the mouse you are
	relativeSpeed = global_position.distance_to(get_global_mouse_position()) + minSpeed
	
	if relativeSpeed > maxSpeed:
		relativeSpeed = maxSpeed
	
	#only move when click
	if Input.is_action_pressed("RightMouseButton") and not aiming_ability:
		$AnimationPlayer.set_current_animation("Walking")
		if distance > 1:
			velocity = move_and_slide(direction*relativeSpeed)
			if round(comparator.x) > 0:
				$Sprite.set_flip_h(true)
			else:
				$Sprite.set_flip_h(false)
	if Input.is_action_just_released("RightMouseButton"):
		$AnimationPlayer.set_current_animation("Default")
	# This is where the actual ability is cast/initiated, no going back!
	if Input.is_action_just_pressed("LeftMouseButton") and aiming_ability and ($AnimationPlayer.get_current_animation() == "Default" or $AnimationPlayer.get_current_animation() == "Walking"):
		#aiming_ability = false
		match selected_ability:
			0:
				$StompWindup.play(.2)
				$AnimationPlayer.play("Sweep",-1,1.5,0)
				#$SweepAimTool.set_visible(false)
			1:
				$StompWindup.play(.2)
				$AnimationPlayer.play("Stomp",-1,1,0)
				#$StompAimTool.set_visible(false)
			2:
				$StompWindup.play(.3)
				#$TreeAimTool.set_visible(false)
				create_tree()
	
	if Input.is_action_just_pressed("RightMouseButton") and aiming_ability:
		clear_ability()
		pass
	
	if Input.is_action_just_pressed("ui_cancel") and aiming_ability:
		clear_ability()
		pass
	
	
	if Input.is_action_just_released("LeftMouseButton") and aiming_ability == false:
		has_released_ability = true
		
	if Input.is_action_just_pressed("Ability1") and ($AnimationPlayer.get_current_animation() == "Walking" or $AnimationPlayer.get_current_animation() == "Default"):
		clear_ability()
		aiming_ability = true
		selected_ability = 0
		$SweepAimTool.set_visible(true)
		#has_released_ability = false
	if Input.is_action_just_pressed("Ability2") and ($AnimationPlayer.get_current_animation() == "Walking" or $AnimationPlayer.get_current_animation() == "Default"):
		clear_ability()
		aiming_ability = true
		selected_ability = 1
		$StompAimTool.set_visible(true)
		#has_released_ability = false
	if Input.is_action_just_pressed("Ability3") and ($AnimationPlayer.get_current_animation() == "Walking" or  $AnimationPlayer.get_current_animation() == "Default"):
		clear_ability()
		aiming_ability = true
		selected_ability = 2
		$TreeAimTool.set_visible(true)
		#has_released_ability = false
		
	handle_ability_aiming()
## End of process

func clear_ability():
	aiming_ability = false
	$SweepAimTool.set_visible(false)
	$StompAimTool.set_visible(false)
	$TreeAimTool.set_visible(false)
	has_released_ability = true
	isAttacking = false
	
func handle_ability_aiming():
	if aiming_ability:
		var aimdist = 1
		match selected_ability:
			0:
				aimdist = 30
			1:
				aimdist = 30
			2: 
				aimdist = 45
		var aimdir = self.get_global_position().direction_to(get_global_mouse_position())
		var aim_spot = aimdir * aimdist
		aim_spot = Vector2(aim_spot.x*2, aim_spot.y)
		$StompAimTool.position = aim_spot
		$SweepAimTool.position = aim_spot
		$SweepAimTool.rotation = aimdir.angle()
		$TreeAimTool.set_global_position(get_global_mouse_position())

func create_stomp():
	#emit_signal("shakeScreen")
	var scene = load("res://Effects/Player_Stomp.tscn")
	var stomp = scene.instance()
	get_tree().get_root().add_child(stomp)
	stomp.set_global_position($StompAimTool.get_global_position())
	isAttacking = true

func end_stomp():
	$AnimationPlayer.play("Default")
	clear_ability()

func create_sweep():
	#emit_signal("shakeScreen")
	var scene = load("res://Effects/Player_Sweep.tscn")
	var sweep = scene.instance()
	get_tree().get_root().add_child(sweep)
	sweep.set_global_position($SweepAimTool.get_global_position())
	sweep.set_global_rotation(self.get_global_position().direction_to(get_global_mouse_position()).angle())
	isAttacking = true

func end_sweep():
	$AnimationPlayer.play("Default")
	clear_ability()

func create_tree():
	if ResourceManager.player.current_hp > 5:
		ResourceManager.player.current_hp -= 5
		ResourceManager.hud.play_dmg_effect()
		var scene = load("res://Objects/Tree.tscn")
		var tree = scene.instance()
		ResourceManager.y_sorter.add_child(tree)
		tree.set_global_position($TreeAimTool.get_global_position())
	clear_ability()

func _on_Regen_timeout():
	current_hp += 1

func _on_Invincibility_timeout():
	isInvinc = false

func look_to_spot(globalPos : Vector2):
	var dirVector = globalPos - self.get_global_position()
	if dirVector.x > 0:
		$Sprite.set_flip_h(true)
	else:
		$Sprite.set_flip_h(false)