extends Node2D

var move_speed = 50
var fly_height = 0
var fly_speed = 0
var fly_dir = Vector2.ZERO
var fly_angle = 1

func set_direction():
	if fly_dir.x > 0:
		$ArrowBody.set_flip_h(true)
		fly_angle = -1

func _physics_process(delta):
	$ArrowBody.set_rotation((fly_speed/8) * fly_angle)
	$ArrowBody.position.y = -fly_height/3
	self.position += fly_dir.normalized() * move_speed * 0.1
	fly_height += fly_speed
	fly_speed -= 0.2
	if fly_height <= 0:
		arrow_impact()

func arrow_impact():
	$ArrowBody.set_rotation(0)
	$ArrowBody.set_frame(11)
	var target = self.get_parent()
	var source = $ArrowBody
	self.remove_child(source)
	target.add_child(source)
	source.set_owner(target)
	source.set_global_position(self.get_global_position())
	queue_free()

func _on_AttackArea_body_entered(body):
	if body.is_in_group("Enemy"):
		body.get_hurt(4)
		queue_free()