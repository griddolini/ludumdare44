extends StaticBody2D

const ATTACKABLE = true

export var max_hp = 3
export var current_hp = 3

func _ready():
	#$Sprite.set_scale(Vector2(rand_range(.8,1.5),rand_range(.8,1.2)))
	pass
	

func take_damage(dmg : float):
	ResourceManager.create_punch_effect(self.get_global_position())
	current_hp -= dmg
	$CPUParticles2D.set_emitting(true)
	if current_hp <= 0:
		self.queue_free()