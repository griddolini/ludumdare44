extends KinematicBody2D

export(NodePath) var target
export var move_speed = 500
export var attack_speed = 0.5
export var range_multiplier = 1
export var current_health = 20
export var active = false
export var sword_dmg = 3
enum weapon {SWORD, BOW}
export(weapon) var my_weapon = weapon.SWORD

var fly_height = 0
var fly_speed = 0
var fly_dir = Vector2.ZERO

var target_node = null
var velocity := Vector2()
var distance_to_target := 0.0
var direction := Vector2()
var movement_dir

# 0 = right, 1 = up, 2 = left, 3 =down
var facing_dir : int = 0
var facing_vector : Vector2 = Vector2.ZERO

enum state {WAIT, MOVE, ATTACK, HURT, AIRBORNE}
var current_state = state.WAIT

onready var ResourceManager = get_node("/root/ResourceManager")

func _ready():
	target_node = get_node(target)

func _physics_process(delta):
	if active:
		direction = (target_node.get_global_position() - self.get_global_position()).normalized()
		distance_to_target = self.get_global_position().distance_to(target_node.get_global_position())
		
		# Move to target, slow down and stop if we are close enough
		if current_state == state.MOVE or current_state == state.WAIT:
			if distance_to_target > move_speed:
				velocity = move_and_slide(direction * move_speed)
				current_state = state.MOVE
			elif distance_to_target > 32.0 * range_multiplier/2:
				velocity = move_and_slide(direction * distance_to_target)
				current_state = state.MOVE
			else:
				velocity = Vector2.ZERO
				current_state = state.WAIT
		# Animate according to our state
		update_animation()
		# If we are allowed, check for targets to swing at
		if current_state != state.HURT and current_state != state.AIRBORNE and current_state != state.ATTACK:
			update_combat()

func update_animation():
	movement_dir = rad2deg(direction.angle())
	
	match current_state:
		state.WAIT:
			#$AnimationPlayer.set_speed_scale(0)
			pass
			
		state.MOVE:
			if movement_dir > 90:
				facing_dir = 3
				facing_vector = Vector2(-2,1)
			elif movement_dir > 0:
				facing_dir = 0
				facing_vector = Vector2(2,1)
			elif movement_dir > -90:
				facing_dir = 1
				facing_vector = Vector2(2,-1)
			else:
				facing_dir = 2
				facing_vector = Vector2(-2,-1)
				
			if facing_dir == 0 or facing_dir == 1:
				$Sprite.set_flip_h(true)
			else:
				$Sprite.set_flip_h(false)
				
			$AnimationPlayer.set_speed_scale(1)
			
			if facing_dir == 0 or facing_dir == 3:
				$AnimationPlayer.play("RunDown")
			else:
				$AnimationPlayer.play("RunUp")
				
		state.ATTACK:
			pass
			
		state.HURT:
			pass
			
		state.AIRBORNE:
			$Sprite.rotate(0.1)
			$Sprite.position.y = -fly_height/3
			self.position += fly_dir.normalized() * move_speed * 0.05
			fly_height += fly_speed
			fly_speed -= 0.2
			if fly_height <= 0:
				fly_speed = 0
				fly_height = 0
				$Sprite.rotation = 0
				current_state = state.WAIT
				if current_health <= 0:
					die()
			pass

func update_combat():
	if(my_weapon == weapon.SWORD):
		var aim_spot = self.get_global_position().direction_to(target_node.get_global_position()) * 12
		aim_spot = Vector2(aim_spot.x*2, aim_spot.y)
		$AttackArea.position = aim_spot
	
	# Check if we are able to attack something
	if my_weapon == weapon.SWORD:
		var bodies = $AttackArea.get_overlapping_bodies()
		for body in bodies:
			if "ATTACKABLE" in body:
				current_state = state.ATTACK
				if rand_range(0,1) > 0.85:
					$GrowlSound.set_pitch_scale(rand_range(.8,1.2))
					$GrowlSound.play()
					
				if facing_dir == 0 or facing_dir == 3:
					$AnimationPlayer.play("AttackDown",-1,attack_speed,0)
				else:
					$AnimationPlayer.play("AttackUp",-1,attack_speed,0)
	elif my_weapon == weapon.BOW:
		if self.get_global_position().distance_to(target_node.get_global_position()) < 18 * range_multiplier:
			current_state = state.ATTACK
			if rand_range(0,1) > 0.85:
				$GrowlSound.set_pitch_scale(rand_range(.8,1.2))
				$GrowlSound.play()
				
			if facing_dir == 0 or facing_dir == 3:
				$AnimationPlayer.play("AttackDown",-1,attack_speed,0)
			else:
				$AnimationPlayer.play("AttackUp",-1,attack_speed,0)

func do_attack():
	$SwingSound.set_pitch_scale(rand_range(.8,1.2))
	$SwingSound.play()
	# At the peak of a sword swing, this will apply damage to other things
	if my_weapon == weapon.SWORD:
		var bodies = $AttackArea.get_overlapping_bodies()
		for body in bodies:
			if "current_hp" in body:
				body.take_damage(sword_dmg)
			elif "current_hp" in body.get_parent():
				body.get_parent().take_damage(3)
			pass
	elif my_weapon == weapon.BOW:
		shoot_arrow()

func end_attack():
	current_state = state.WAIT
	
func get_hurt(dmg, explosive=Vector2.ZERO):
	ResourceManager.create_punch_effect(self.get_global_position(), true)
	current_health -= dmg
	if explosive != Vector2.ZERO:
		var explosion_dist = self.get_global_position().distance_to(explosive)
		fly_dir = self.get_global_position() - explosive
		fly_height = 1
		fly_speed = clamp(explosion_dist, 2, 8)
		current_state = state.AIRBORNE
		$AnimationPlayer.play("Flying",-1,0,0)
	elif current_health <= 0:
		die()
		
func shoot_arrow():
	var scene = load("res://Objects/Arrow.tscn")
	var arrow = scene.instance()
	ResourceManager.y_sorter.add_child(arrow)
	arrow.set_global_position(self.get_global_position())
	arrow.fly_dir = self.get_global_position().direction_to(target_node.get_global_position()).rotated(rand_range(-0.25,0.25))
	arrow.fly_speed = rand_range(0.6,0.7) * range_multiplier
	arrow.set_direction()
		
func die():
	var scene = load("res://Effects/DeadGobulin.tscn")
	var dead = scene.instance()
	ResourceManager.y_sorter.add_child(dead)
	dead.set_global_transform(Transform2D(0, self.get_global_position()))
	dead.set_scale(self.get_scale())
	dead.z_index = -128
	if facing_dir == 1 or facing_dir == 2:
		dead.set_frame(13)
	if facing_dir == 3 or facing_dir == 2:
		dead.set_flip_h(false)
	queue_free()