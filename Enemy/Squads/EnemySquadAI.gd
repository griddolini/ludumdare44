extends Node2D

var activated = false
signal squad_defeated

func _ready():
	pass

# Check if we need to change objectives, etc
func _on_Timer_timeout():
	# Eventually this will pick a building or player or something
	$MoveObjective.global_position = ResourceManager.player.global_position
	
	if self.get_child_count() == 2:
		emit_signal("squad_defeated")
		print("squad defeated")
		queue_free()
	
	if not activated:
		activated = true
		for child in self.get_children():
			if child.is_in_group("Enemy"):
				child.active = true

