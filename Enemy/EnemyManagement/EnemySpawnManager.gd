extends Node2D

export var current_wave = 0
export var spawn_time = 1
export var wave_time = 5

var round_in_progress = false

var spawn_points = []
var next_spawn_point : int = 0

var spawned_squads : int = 0
var active_squads : int = 0

var squad_type_1 = preload("res://Enemy/Squads/EnemySquad_Basic_1.tscn")
var squad_type_2 = preload("res://Enemy/Squads/EnemySquad_GatlingArchers.tscn")
var squad_type_3 = preload("res://Enemy/Squads/EnemySquad_BigBoi.tscn")

onready var ResourceManager = get_node("/root/ResourceManager")

func _ready():
	# Add however many spawn points exist
	for child in self.get_children():
		if child is Sprite:
			spawn_points.append(child)

func spawn_squad():
	if(spawned_squads < current_wave) and round_in_progress:
		
		# Choose type to spawn
		randomize()
		var scene
		var rand = randi()%11+1
		if rand <= 3:
			scene = squad_type_1
		elif rand >= 4 and rand <= 6:
			scene = squad_type_2
		else:
			scene = squad_type_3
		
		# Spawn the squad
		var squad = scene.instance()
		ResourceManager.y_sorter.add_child(squad)
		squad.set_global_position(spawn_points[next_spawn_point].get_global_position())
		squad.connect("squad_defeated", self, "squad_killed")
		spawned_squads += 1
		active_squads += 1
		# Advance through the spawn points
		if next_spawn_point < spawn_points.size()-1:
			next_spawn_point += 1
		else:
			next_spawn_point = 0
		$SpawnSpacer.start(spawn_time)

func squad_killed():
	active_squads -= 1
	if active_squads == 0 and round_in_progress:
		print("wave completed")
		$AudioStreamPlayer2D.play(0)
		round_in_progress = false
		$WaveInterval.start(5)

func start_next_wave():
	if ResourceManager.total_buildings > 1:
		current_wave += 1
		ResourceManager.hud.set_wave_count(str(current_wave))
		round_in_progress = true
		spawned_squads = 0
		print("starting wave ", str(current_wave))
		$SpawnSpacer.start(1)
	else:
		$WaveInterval.start(5)