extends CanvasLayer

onready var ResourceManager = get_node("/root/ResourceManager")

func _ready():
	ResourceManager.hud = self

func _process(delta):
	
	var player = ResourceManager.player 

	if player.get_node("AnimationPlayer").current_animation == "Sweep":
		$SlashIcon.modulate.a = 0.3
		$StompIcon.modulate.a = 0.3
		$TreeIcon.modulate.a = 0.3
	if player.get_node("AnimationPlayer").current_animation == "Stomp":
		$StompIcon.modulate.a = 0.3
		$SlashIcon.modulate.a = 0.3
		$TreeIcon.modulate.a = 0.3
	
	if not player.aiming_ability and player.get_node("AnimationPlayer").get_current_animation() == "Walking" or player.get_node("AnimationPlayer").get_current_animation() == "Default":
		$SlashIcon.modulate.a = 1
		$StompIcon.modulate.a = 1
		$TreeIcon.modulate.a = 1
	
	if player.aiming_ability and not player.get_node("AnimationPlayer").is_playing():
		match player.selected_ability:
			0:
				$SlashIcon.modulate.r = 10
				$StompIcon.modulate.r = 1
				$TreeIcon.modulate.r = 1
			1:
				$SlashIcon.modulate.r = 1
				$StompIcon.modulate.r = 10
				$TreeIcon.modulate.r = 1
			2:
				$SlashIcon.modulate.r = 1
				$StompIcon.modulate.r = 1
				$TreeIcon.modulate.r = 10
	elif not player.aiming_ability:
		$SlashIcon.modulate.r = 1
		$StompIcon.modulate.r = 1
		$TreeIcon.modulate.r = 1
	
	$MAXHP.set_text(var2str(ResourceManager.player.max_hp))
	
	$FE.set_text(var2str(ResourceManager.total_buildings) + "%")

func set_hp_text(hp):
	$HP.set_text(hp)

func set_wave_count(count):
	$Panel/Label.set_text("Wave: " + count)

func play_dmg_effect():
	$HP/DamageParticles.restart()
	$HP/DamageParticles.set_emitting(true)
	
func play_heal_effect():
	$HP/HealParticles.set_emitting(true)
	
func _input(event):
	if Input.is_action_pressed("ui_cancel"):
		get_tree().paused = true
		$Pause.show()

func _on_Play_pressed():
	get_tree().paused = false
	$Pause.hide()

func _on_Quit_pressed():
	get_tree().change_scene("res://Stages/MainMenu.tscn")

func _on_Controls_pressed():
	$Popup.show()


func _on_BackToGame_pressed():
	$Popup.hide()
