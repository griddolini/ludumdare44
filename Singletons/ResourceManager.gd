extends Node

var camera
var player
var y_sorter
var total_buildings = 0
var hud

func create_punch_effect(globalPos : Vector2, useBlood : bool = false):
	var scene = load("res://Effects/DamageEffect.tscn")
	var hit = scene.instance()
	y_sorter.add_child(hit)
	hit.set_global_position(globalPos)
	hit.use_blood = useBlood
	hit.deployed()