extends KinematicBody2D

const ATTACKABLE = true

export var max_hp = 100
export var current_hp = 100

func _ready():
	pass

func take_damage(dmg : float):
	ResourceManager.create_punch_effect(self.get_global_position())
	if self.is_in_group("Player"):
		if not self.isInvinc:
			ResourceManager.hud.play_dmg_effect()
			current_hp -= dmg
			self.get_node("Invincibility").start()
			self.isInvinc = true
	else:
		current_hp -= dmg