extends Node2D

const TYPE = "BUILDING"
const MAXLEVEL = 3
const ATTACKABLE = true

var shooting_arrows = false
var arrow_dir = Vector2.ZERO

var isUpgradable = false
var upgradeLevel = 0
var farmReward1 = 30
var farmReward2 = 40
var farmReward3 = 50
var cost = 5
var current_hp = 2

var loadedSprite = preload("res://Art/Sapling.png")
var loadedSprite2 = preload("res://Art/Sapling2.png")

export(String, "none", "Farm", "Tavern", "ArrowTower") var buildingType

onready var outline = get_node("Sprite/Outline")
onready var sprite = get_node("Sprite")
onready var ResourceManager = get_node("/root/ResourceManager")
onready var cooldown = get_node("UpgradeCooldown")
onready var particles = get_node("CPUParticles2D")
onready var smallBlocker = get_node("SmallBlocker")
onready var tavernBlocker = get_node("TavernBlocker")
onready var farmBlocker = get_node("FarmBlocker")
onready var farmTimer = get_node("FarmTimer")
onready var area = get_node("Area2D")
onready var farmParticles = get_node("FarmWorkingParticles")
onready var farmText = get_node("FarmWorkingText")

func _ready():
	randomize()
	if randi()%11 + 1 < 5:
		sprite.texture = loadedSprite
		outline.texture = loadedSprite
	else:
		sprite.texture = loadedSprite2
		outline.texture = loadedSprite2

func _process(delta):
	
	if upgradeLevel == 0:
		outline.scale = sprite.scale * 1.1
	else:
		outline.scale = sprite.scale * 0.4
	
	if current_hp == 0:
		queue_free()
		ResourceManager.total_buildings -= 1
	
	# Handle states, maybe rewrite into state machine
	if isUpgradable and ResourceManager.player.current_hp > cost:
		outline.visible = true
		
		# lazy if-statement, makes sure space, timer = 0 and level are all valid before showing particles
		if ResourceManager.player.actionButton and cooldown.is_stopped() and upgradeLevel < MAXLEVEL:
			ResourceManager.player.current_hp -= cost
			ResourceManager.hud.get_node("Cost").text = var2str(cost*-1)
			ResourceManager.hud.get_node("Cost").visible = true
			ResourceManager.hud.get_node("InfoPanel/RichTextLabel").add_text("\n" + "Upgraded " + var2str(buildingType) + " for " + var2str(cost))
			upgradeLevel += 1
			ResourceManager.total_buildings += 2
			particles.emitting = true
			cooldown.start()
			current_properties(buildingType)
			area.scale = Vector2(2,2)
			particles.scale = Vector2(2,2)
			$UpgradeSound.set_pitch_scale(rand_range(.4,1.8))
			$UpgradeSound.play()
			ResourceManager.hud.play_dmg_effect()

		# Ughhh bad but works
		if ResourceManager.player.actionButton and upgradeLevel == MAXLEVEL and cooldown.is_stopped() and farmTimer.is_stopped() and buildingType == "Farm":
				ResourceManager.player.current_hp -= cost
				current_properties(buildingType)
				particles.emitting = true
				cooldown.start()
				$UpgradeSound.set_pitch_scale(rand_range(.4,1.8))
				$UpgradeSound.play()
				ResourceManager.hud.play_dmg_effect()
				ResourceManager.hud.get_node("InfoPanel/RichTextLabel").add_text("\n" + "Reinvested in " + var2str(buildingType) + " for " + var2str(cost) + "\n")
	else:
		outline.visible = false
	
	# If you're on cooldown, be kinda see through
	if not cooldown.is_stopped():
		sprite.modulate.a = 0.8
		outline.modulate.a = 0.8
	elif cooldown.is_stopped():
		sprite.modulate.a = 1
		outline.modulate.a = 1

# called once, on enter
func _on_Area2D_area_shape_entered(area_id, area, area_shape, self_shape):
	if area != null:
		if area.get_parent() != null:
			if area.get_parent().get("TYPE") == "Player":
				isUpgradable = true

# ditto, but on leave
func _on_Area2D_area_shape_exited(area_id, area, area_shape, self_shape):
	if area != null:
		if area.get_parent() != null:
			if area.get_parent().get("TYPE") == "Player":
				isUpgradable = false

# So you dont upgrade too fast, match this timer to lifetime of particles
func _on_UpgradeCooldown_timeout():
	particles.emitting = false
	
func current_properties(type):
	
	# the most powerful statement...
	match type:
		"Farm":
			match upgradeLevel:
				1:
					sprite.texture = load("res://Art/Farm.png")
					outline.texture = load("res://Art/Farm.png")
					sprite.scale = Vector2(3,3)
					outline.scale = sprite.scale * 0.4
					call_deferred('blockerGrow')
					farmParticles.emitting = true
					ResourceManager.hud.get_node("Cost").text = var2str(cost*-1)
					ResourceManager.hud.get_node("Cost").visible = true
					$CostTimer.start()
					#farmText.visible = true
					cost = 10
				2:
					farmParticles.emitting = true
					cost = 20
					ResourceManager.hud.get_node("Cost").text = var2str(cost*-1)
					ResourceManager.hud.get_node("Cost").visible = true
					$CostTimer.start()
				3:
					farmParticles.emitting = true
					cost = 30
					ResourceManager.hud.get_node("Cost").text = var2str(cost*-1)
					ResourceManager.hud.get_node("Cost").visible = true
					$CostTimer.start()
			
			if farmTimer.is_stopped():
				farmTimer.start()
		"Tavern":
			match upgradeLevel:
				1:
					sprite.texture = load("res://Art/Tavern.png")
					outline.texture = load("res://Art/Tavern.png")
					sprite.scale = Vector2(3,3)
					outline.scale = sprite.scale * 0.4
					call_deferred('blockerGrow')
					cost = 10
					ResourceManager.player.livingWorkers += 10
					$Workers.visible = true
					ResourceManager.hud.get_node("Cost").text = var2str(cost*-1)
					ResourceManager.hud.get_node("Cost").visible = true
					$CostTimer.start()
					for worker in $Workers.get_children():
						worker.get_node("CollisionShape2D").disabled = false
					cost = 10
				2:
					ResourceManager.player.livingWorkers += 10
					ResourceManager.hud.get_node("Cost").text = var2str(cost*-1)
					ResourceManager.hud.get_node("Cost").visible = true
					$CostTimer.start()
					cost = 30
				3:

					ResourceManager.player.livingWorkers += 20
					ResourceManager.hud.get_node("Cost").text = var2str(cost*-1)
					ResourceManager.hud.get_node("Cost").visible = true
					$CostTimer.start()
					cost = 40
		"ArrowTower":
			match upgradeLevel:
				1:
					sprite.texture = load("res://Art/arrowtower.png")
					outline.texture = load("res://Art/arrowtower.png")
					sprite.scale = Vector2(3,3)
					outline.scale = sprite.scale * 0.4
					call_deferred('blockerGrow')
					ResourceManager.hud.get_node("Cost").text = var2str(cost*-1)
					ResourceManager.hud.get_node("Cost").visible = true
					$CostTimer.start()
					cost = 20
				2:
					ResourceManager.hud.get_node("Cost").text = var2str(cost*-1)
					ResourceManager.hud.get_node("Cost").visible = true
					$CostTimer.start()
					cost = 40
				3:
					ResourceManager.hud.get_node("Cost").text = var2str(cost*-1)
					ResourceManager.hud.get_node("Cost").visible = true
					$CostTimer.start()
					cost = 60
		"none":
			match upgradeLevel:
				1:
					cost = 10
				2:
					cost = 20
				3:
					cost = 30
			
			# IDEALLY THIS CODE SHOULD NEVER BE CALLED IT IS A FALLBACK
			print("You didnt configure me, I have no type!")
			sprite.texture = load("res://icon.png")
			outline.texture = load("res://icon.png")
			sprite.scale = Vector2(2,2)
			outline.scale = sprite.scale * 0.4
			call_deferred('blockerGrow')

func blockerGrow():
	match buildingType:
		"Farm":
			farmBlocker.get_node("CollisionPolygon2D").disabled = false
			$LargeOccluder.visible = true
		"Tavern":
			tavernBlocker.get_node("CollisionPolygon2D").disabled = false
			$LargeOccluder.visible = true
		"ArrowTower":
			tavernBlocker.get_node("CollisionPolygon2D").disabled = false
			$LargeOccluder.visible = true
	
	smallBlocker.get_node("CollisionPolygon2D").disabled = true
	$SmallOccluder.visible = false

func _on_FarmTimer_timeout():
	match upgradeLevel:
				1:
					ResourceManager.player.current_hp += farmReward1
					ResourceManager.hud.play_heal_effect()
					farmParticles.emitting = false
					#farmText.visible = false
					ResourceManager.hud.get_node("InfoPanel/RichTextLabel").add_text("\n" + "Got " + var2str(farmReward1) + "HP from " + var2str(buildingType))
				2:
					ResourceManager.player.current_hp += farmReward2
					ResourceManager.hud.play_heal_effect()
					farmParticles.emitting = false
					#farmText.visible = false
					ResourceManager.hud.get_node("InfoPanel/RichTextLabel").add_text("\n" + "Got " + var2str(farmReward2) + "HP from " + var2str(buildingType))
				3:
					ResourceManager.player.current_hp += farmReward3
					ResourceManager.hud.play_heal_effect()
					farmParticles.emitting = false
					#farmText.visible = false
					ResourceManager.hud.get_node("InfoPanel/RichTextLabel").add_text("\n" + "Got " + var2str(farmReward3) + "HP from " + var2str(buildingType))

func _on_KillTimer_timeout():
	if upgradeLevel == 0:
		queue_free()

func take_damage(dmg):
	current_hp -= 1
	ResourceManager.create_punch_effect(self.get_global_position())

func _on_CostTimer_timeout():
	ResourceManager.hud.get_node("Cost").visible = false

func shoot_arrow(dir):
	var scene = load("res://Objects/Good_Arrow.tscn")
	var arrow = scene.instance()
	ResourceManager.y_sorter.add_child(arrow)
	arrow.set_global_position(self.get_global_position())
	arrow.fly_dir = dir.rotated(rand_range(-.5,.5))
	arrow.fly_speed = rand_range(0.6,0.7) * 5
	arrow.set_direction()
	$ArrowShot.play(0)
	if shooting_arrows:
		$ArrowReload.start(.75 - (upgradeLevel / 10))
		
func _on_Area2D2_body_entered(body):
	if buildingType == "ArrowTower" and upgradeLevel > 0 and body.is_in_group("Enemy") and shooting_arrows == false:
		if body.active:
			arrow_dir = self.get_global_position().direction_to(body.get_global_position())
			shooting_arrows = true
			$ArrowBurst.start(1 + upgradeLevel)
			shoot_arrow(arrow_dir)
	else:
		pass


func _on_ArrowBurst_timeout():
	print("Timed out")
	shooting_arrows = false


func _on_ArrowReload_timeout():
	shoot_arrow(arrow_dir)
