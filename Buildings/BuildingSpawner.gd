extends Node2D
export var spawn_range : float = 200

var object = preload("res://Buildings/BaseBuilding.tscn").instance()
var object2 = preload("res://Buildings/BaseBuilding.tscn")
var object3 = preload("res://Buildings/BaseBuilding.tscn").instance()

var numberObjects = 0
var check = preload("res://Buildings/BaseBuilding.tscn")
var spawned
var checking_for_free_space = false

var i = 0

onready var ResourceManager = get_node("/root/ResourceManager")

func _physics_process(delta):
	if checking_for_free_space:
		var overlaps = $SpawnChecker.get_overlapping_areas()
		if overlaps.size() == 0:
			checking_for_free_space = false
			spawn_building()
		else:
			randomize()
			var randVect = Vector2(rand_range(-spawn_range, spawn_range), rand_range(-spawn_range, spawn_range))
			$SpawnChecker.set_global_position(randVect)

func _on_SpawnTimer_timeout():
	checking_for_free_space = true
	
func spawn_building():
	spawned = object2.instance()
	var rand = randi()%3 + 1
	randomize()
	match rand:
		1:
			spawned.buildingType = "Farm"
		2:
			spawned.buildingType = "Tavern"
		3:
			spawned.buildingType = "ArrowTower"
	
	spawned.position = $SpawnChecker.get_global_position()
	spawned.scale = Vector2(0.4, 0.4)
	
	if ResourceManager.total_buildings < 100:
		ResourceManager.y_sorter.add_child(spawned)
