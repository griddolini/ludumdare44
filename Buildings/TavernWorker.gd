extends "res://SharedScripts/Attackable.gd"

var move = Vector2()

func _ready():
	current_hp = 1
	
func _process(delta):
	move_and_slide(move* 2)
	

func _on_MoveTimer_timeout():
	move = Vector2(rand_range(-10, 10), rand_range(-10,10))
	if move.x < 0:
		$Sprite.set_flip_h(false)
	else:
		$Sprite.set_flip_h(true)
		
	return move

func take_damage(dmg):
	ResourceManager.create_punch_effect(self.get_global_position())
	queue_free()